import { ref, computed} from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth',()=>{
    const currentUser = ref<User>({
        id: 1,
        email: 'eiei123@gmail.com',
        password: '1234',
        fullname: 'อิอิ อุอุ',
        gender: 'male',
        roles: ['user']
    })
    return { currentUser }
})